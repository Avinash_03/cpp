//Array of memory delete karnyasathi  delete[] vapartat
#include <iostream>
class Demo{
	int *ptrArray=NULL;
	public:
	Demo(){
		ptrArray=new int[50];
		std::cout<<"In constructor"<<std::endl;
	}
	~Demo(){
		delete[] ptrArray;
		std::cout<<"In Destructor"<<std::endl;
	}
};
int main(){
	{
		Demo obj;
	}
	std::cout<<"End Main"<<std::endl;
	return 0;
}
