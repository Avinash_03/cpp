#include <iostream>
void fun(int x,int y){
	std::cout<<x" "<<y<<std::endl;
}
void fun(int x,float y){
	std::cout<<x" "<<y<<std::endl;
}
void fun(float x,float y){
	std::cout<<x" "<<y<<std::endl;
}
int main(){
	fun(10,20);			//when multiple methods given then first cheack all data match or not then cheack single data match or not when no not 
	fun(10,20.5f);			//match then it give arror ambiguous				
	fun(10.5f,20.5f);		//when not multiple option then call them 
	fun(10.5,20.5);			//ambiguous
	return 0;
}
