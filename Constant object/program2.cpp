#include <iostream>
class Demo{
	public:
	int x=10;
	Demo(){
		std::cout<<"In constructor"<<std::endl;
		std::cout<<x<<std::endl;
	}
	Demo(int x){
		std::cout<<"In Paramiterized constructor"<<std::endl;
		this->x=x;
		std::cout<<x<<std::endl;
		Demo();					//Temprory object create hoto
	}
	~Demo(){
		std::cout<<"In Destructor"<<std::endl;
	}
};
int main(){
	Demo obj(50);
	std::cout<<"End main"<<std::endl;
	return 0;
}
