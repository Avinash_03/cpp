#include<iostream>
class Demo{
	public:
	Demo(){
		std::cout<<"No-args"<<std::endl;
	}
	Demo(int x){
		std::cout<<"Para"<<std::endl;
	}
	Demo(Demo &obj1){
		std::cout<<"Copy"<<std::endl;
	}
	void fun(){
		std::cout<<x<<std::endl;
		std::cout<<this.x<<std::endl;
	}
};
int main(){
	Demo obj1;			//No args
	Demo obj3 = obj1;		//Copy
	std::cout<<obj1.x<<std::endl;
	std::cout<<obj3.x<<std::endl;
	return 0;
}
