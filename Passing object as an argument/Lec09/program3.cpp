#include <iostream>
class Demo{
	int x=10;
	int y=20;
	public:
	Demo(){
		std::cout<<"No-args"<<std::endl;
	}
	Demo(Demo &ref){
		std::cout<<"Copy"<<std::endl;
	}
	Demo(int x,int y){
		this->x=x;
		this->y=y;
		std::cout<<"Para"<<std::endl;
	}
	void access(){
		std::cout<<x<<y<<std::endl;
	}
	Demo &info(Demo &obj){		//Demo &info(Demo obj)		2 call to copy constructor
		obj.x=700;
		obj.y=800;
		return obj;
	}
};
int main(){
	Demo obj;
	obj.access();
	Demo obj1(100,200);
	obj1.access();
	Demo obj2 =obj1.info(obj);
	obj2.access();
	return 0;
}
