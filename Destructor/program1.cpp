#include <iostream>
class Demo{
	public:
	Demo(){
		std::cout<<"In constructor"<<std::endl;
	}
	~Demo(){
		std::cout<<"In destructor"<<std::endl;
	}
};
int main(){
	Demo obj;
	Demo *obj2=new Demo();
	std::cout<<"In constructor"<<std::endl;
	delete obj2;
	return 0;
}
